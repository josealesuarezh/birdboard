<?php

namespace Tests\Feature;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Facades\Tests\Setup\ProjectFactory;
use Tests\TestCase;

class ProjectTasksTest extends TestCase
{
    use RefreshDatabase, WithFaker;


    /**  @test */
    public function guest_cannot_add_tasks_to_projects(){
        $project = Project::factory()->create();

        $this->post($project->path() . '/tasks')->assertRedirect('login');
    }

    /** @test */
    public function only_the_owner_of_a_project_can_add_tasks()
    {

        $this->signIn();

        $project = Project::factory()->create();

        $this->post($project->path() . '/tasks', ['body' => 'Test Task'])
            ->assertStatus(403);

        $this->assertDatabaseMissing('project_tasks', ["body" => 'Test Task']);
    }

    /** @test */
    public function only_the_owner_of_a_project_can_update_tasks()
    {
        $this->signIn();

        $project = ProjectFactory::withTasks(1)->create();

        $this->patch($project->tasks[0]->path(),[
            'body' => 'update body',
            'completed' => true
        ])->assertStatus(403);

        $this->assertDatabaseMissing('project_tasks',[
            'body' => 'update body',
            'completed' => true
        ]);
    }

    /**  @test */
    public function a_project_can_have_tasks()
    {
        $project = ProjectFactory::create();

        $this->actingAs($project->owner)
            ->post($project->path() . '/tasks', [
            "body" => 'Test Task'
        ]);

        $this->get($project->path())->assertSee('Test Task');
    }

    /**  @test */
    public function a_task_requires_a_body()
    {
        $project = ProjectFactory::create();

        $attributes = Task::factory()->raw(['body' => '']);

        $this->actingAs($project->owner)
            ->post($project->path() . '/tasks', $attributes)
            ->assertSessionHasErrors('body');
    }

    /**  @test */
    public function a_task_can_be_updated()
    {
        $project = ProjectFactory::withTasks(1)
            ->create();

        $this->actingAs($project->owner)
            ->patch($project->tasks[0]->path(),[
            'body' => 'update body',
        ]);

        $this->assertDatabaseHas('project_tasks',[
            'body' => 'update body',
        ]);
    }

    /**  @test */
    public function a_task_can_be_completed()
    {
        $project = ProjectFactory::withTasks(1)
            ->create();

        $this->actingAs($project->owner)
            ->patch($project->tasks[0]->path(),[
                'body' => 'update body',
                'completed' => true
            ]);

        $this->assertDatabaseHas('project_tasks',[
            'body' => 'update body',
            'completed' => true
        ]);
    }

    /**  @test */
    public function a_task_can_be_mark_as_incomplete()
    {
        $project = ProjectFactory::withTasks(1)
            ->create();

        $this->actingAs($project->owner)
            ->patch($project->tasks[0]->path(),[
                'body' => 'update body',
                'completed' => true
            ]);

        $this->patch($project->tasks[0]->path(),[
                'body' => 'update body',
                'completed' => false
            ]);

        $this->assertDatabaseHas('project_tasks',[
            'body' => 'update body',
            'completed' => false
        ]);
    }

}
