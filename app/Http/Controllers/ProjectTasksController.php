<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;

class ProjectTasksController extends Controller
{
    public function store(Project $project)
    {
        $this->authorize('update', $project);

        request()->validate(['body' => 'required']);

        $project->addTask(\request('body'));

        return redirect($project->path());
    }

    public function update(Project $project, Task $projectTask)
    {
        $this->authorize('update', $projectTask->project);

        $projectTask->update(request()->validate(['body' => 'required']));

        request('completed') ? $projectTask->complete() : $projectTask->incomplete();

        return redirect($project->path());
    }
}
