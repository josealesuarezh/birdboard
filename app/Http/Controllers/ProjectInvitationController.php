<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;

class ProjectInvitationController extends Controller
{
    public function store(Project $project)
    {
        $this->authorize('manage',$project);

        \request()->validateWithBag('invitations',[
            'email' => 'required|exists:users,email'
        ],[
            'email.exists' => 'The user that you are inviting must have a Birdboard account.'
        ]);

        $user = User::whereEmail(\request('email'))->first();
        $project->invite($user);

        return redirect($project->path());
    }
}
