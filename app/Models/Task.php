<?php

namespace App\Models;

use App\Traits\RecordActivity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory, RecordActivity;

    protected $table = 'project_tasks';

    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var string[]
     */
    protected $casts = [
      'completed' => 'boolean'
    ];

    protected static $recordableEvents =  ['created','deleted'];

    protected $touches = ['project'];

    /**
     * This task path
     *
     * @return string
     */
    public function path(){
        return "/projects/{$this->project->id}/tasks/{$this->id}";
    }

    /**
     * Mark a task as complete
     */
    public function complete(){
        $this->update(['completed' => true]);

        $this->recordActivity('completed_task');
    }

    /**
     * Mark a task as incomplete
     */
    public function incomplete(){
        $this->update(['completed' => false]);

        $this->recordActivity('uncompleted_task');
    }

    public function project(){
        return $this->belongsTo(Project::class);
    }

}
