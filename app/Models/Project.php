<?php

namespace App\Models;

use App\Traits\RecordActivity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Project extends Model
{
    use HasFactory, RecordActivity;

    /**
     * Attributes to guard against mass assignment
     *
     * @var array
     */
    protected $guarded = [];


    /**
     * The path to the project
     *
     * @return string
     */
    public function path(): string
    {
        return "/projects/{$this->id}";
    }


    public function invite(User $user)
    {
        return $this->members()->attach($user);
    }



    /**
     * Add a task to the project
     *
     * @param $body
     * @return Model
     */
    public function addTask($body)
    {
        return $this->tasks()->create(compact('body'));
    }

    /**
     * The owner of the project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    /**
     * The tasks associated with the project
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    /**
     * Record activity for a project
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activity()
    {
        return $this->hasMany(Activity::class)->latest();
    }

    public function members()
    {
        return $this->belongsToMany(User::class,'projects_members')->withTimestamps();
    }
}
