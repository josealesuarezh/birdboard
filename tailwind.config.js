module.exports = {
    purge: [],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            shadow: {
                default: '0 0 5px 0 rgba(0, 0, 0, 0.08)'
            },
            colors:{
                'grey':'rgba(0, 0, 0, 0.4)',
                'grey-light': '#F5F6F9',
                'white': '#FFFFFF'
            }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
