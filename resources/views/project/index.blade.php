@extends('layouts.app')

@section('content')
    <header class="flex items-center mb-3 py-4">
        <div class="flex justify-between w-full items-center">
            <h2 class="text-grey font-normal">My Projects</h2>

            <a href="/projects/create" class="button py-2 px-4 text-sm">Add Project</a>
        </div>

    </header>

    <main class="lg:flex lg:flex-wrap -mx-3">
        @forelse($projects as $project)
            <div class="lg:w-1/3 px-3 pb-6">
                @include('project.card')
            </div>
        @empty
            <div>No projects yet</div>
        @endforelse
    </main>
@endsection

