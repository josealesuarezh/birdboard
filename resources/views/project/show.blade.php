@extends('layouts.app')

@section('content')
    <header class="flex items-center mb-3 py-4">
        <div class="flex justify-between w-full items-center">
            <p class="text-grey font-normal">
                <a href="/projects">My Projects</a> / {{ $project->title }}
            </p>

            <div class="flex items-center">
                @foreach($project->members as $member)
                    <img class="rounded-full w-8 mr-2" src="https://gravatar.com/avatar/{{ md5($member->email) }}?s=60"
                         alt="{{ $member->name }}'s avatar">
                @endforeach

                <img class="rounded-full w-8 mr-2"
                     src="https://gravatar.com/avatar/{{ md5($project->owner->email) }}?s=60"
                     alt="{{ $project->owner->name }}'s avatar">

                <a href="{{ $project->path() }}/edit" class="button ml-4 py-2 px-4 text-sm">Edit Project</a>
            </div>
        </div>

    </header>
    <main>
        <div class="lg:flex -mx-3 mb-6">
            <div class="lg:w-3/4 px-3">
                <div class="mb-8">
                    <h2 class="text-grey font-normal text-lg mb-3">Tasks</h2>

                    @foreach($project->tasks as $task)
                        <div class="card mb-3">
                            <form action="{{$task->path()}}" method="POST">
                                @method("PATCH")
                                @csrf
                                <div class="flex items-center">
                                    <input type="text" class="w-full {{ $task->completed ? 'text-grey' : '' }}"
                                           name="body" value="{{ $task->body }}">
                                    <input type="checkbox" class="text-lg" name="completed"
                                           onchange="this.form.submit()" {{ $task->completed ? 'checked' : '' }}>
                                </div>
                            </form>
                        </div>
                    @endforeach
                    <div class="card mb-3">
                        <form action="{{$project->path().'/tasks'}}" method="POST">
                            @csrf
                            <input class="w-full" type="text" name="body" placeholder="Add a new tasks...">
                        </form>
                    </div>
                </div>

                <div>
                    <h2 class="text-grey font-normal text-lg mb-3">General Notes</h2>

                    <form method="POST" action="{{ $project->path() }}">
                        @csrf
                        @method('PATCH')
                        <textarea
                            name="notes"
                            class="card w-full mb-4"
                            style="min-height: 150px"
                            placeholder="Anything special that you want to make a note of?">
                            {{$project->notes}}
                        </textarea>
                        <button type="submit" class="button">Save</button>
                    </form>
                    @include('errors')
                </div>
            </div>
            <div class="lg:w-1/4 px-3">
                @include('project.card')
                @include('project.activity.card')
                @can('manage',$project)
                    @include('project.invite')
                @endcan
            </div>

        </div>
    </main>


@endsection
