<div class="card flex flex-col" style="height: 200px">
    <h3 class="font-normal text-xl py-4 -ml-5 border-l-4 border-blue-400 pl-4 mb-3">
        <a href="{{ $project->path() }}">{{ $project->title }}</a>
    </h3>

    <div class="text-grey mb-6 flex-1">
        {{Illuminate\Support\Str::limit($project->description, 100)}}
    </div>
    @can('manage', $project)
        <footer>
            <form action="{{ $project->path() }}" method="POST" class="text-right align-bo">
                @csrf
                @method('DELETE')
                <button type="submit" class="text-xs">
                    Delete
                </button>
            </form>
        </footer>
    @endcan
</div>
