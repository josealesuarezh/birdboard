@extends('layouts.app')

@section('content')

    <div class="card w-1/2 mx-auto text-lg">
        <div class="my-6 mx-8">
            <div class="w-full mb-8 text-4xl text-center">{{ __('Login') }}</div>

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="my-3">
                    <label for="email" class="">{{ __('E-Mail Address') }}</label>

                    <div class="mt-2">
                        <input id="email" type="email" class="w-full bg-transparent border border-grey-light rounded @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="my-3">
                    <label for="password" class="mb-1">{{ __('Password') }}</label>

                    <div class="mt-2">
                        <input id="password" type="password" class="w-full bg-transparent border border-grey-light  rounded @error('password') is-invalid @enderror"
                               name="password" required autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="mt-5 mb-9">
                    <div class="my-3">
                        <input class="form-check-input" type="checkbox" name="remember"
                               id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                    <div class="my-3">
                        <button type="submit" class="button">
                            {{ __('Login') }}
                        </button>

                        @if (Route::has('password.request'))
                            <a class="text-grey ml-2 text-sm" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>
                </div>

            </form>
        </div>

    </div>

@endsection
